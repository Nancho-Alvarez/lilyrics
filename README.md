# Lilyrics

Musescore 3.x plugin to apply lyrics in lilypond format.

It can be used also as a general lyrics text editor.

The lyrics in the score are linked with the text editor but only in one direction: if you click a lyric syllable in the score, the corresponding syllable is automatically highlighted in the text area. This feature uses a function available in MuseScore 3.6 onwards.

Tested in version 3.6.2

## Installation

Copy lilyrics.qml in the plugins directory.
See [MuseScore documentation](https://musescore.org/en/handbook/3/plugins#installation) for details.

## Basic usage

### Single voice
Write in the text window some lyrics with the lilypond syntax, and press the **Paste** button.

Example: ``Ky -- ri -- _ e``

You can choose the staff, voice and verse where to apply the lyrics using the controls in the bottom, or just selecting the appropriate element in the score.

### Several voices

To paste several voices at the same time, enclose them by curly braces. Text outside braces is ignored.

Example: ``{Ky -- ri -- _ e} {Chri -- ste __ _ _ }``


## Lilypond lyrics notation

Only a reduced subset of the lilypond lyrics features are supported. In particular, markups are not supported.

* **hyphens:**
  To separate two syllables of the same word, use `` -- `` (two hyphens surrounded by spaces). Syllables of different words are separated by a normal space.
  
  Example: ``Ky -- ri -- e e -- lei -- son``

* **melisma:**
  To indicate that a syllable should be sung over more than one note, use `` _ `` (one underscore surrounded by spaces) for each additional note in the melisma.

  Example: ``Chri -- _ ste _ _``

  "Chri" is sung over two notes and "ste" is sung over three notes.

* **line extenders:**
  At the end of a word, if you want a line extending the last syllable use `` __ `` (two underscores surrounded by spaces).

  Example: ``Chri -- ste __ _ _ _``

  The syllable "ste" is extended along the next three notes	creating a melisma of four notes.

* **elision:** 
  _Elision_, or more generally _synalepha_, is the union of the last syllable of one word to the first of the next word.  It is very common in Italian and Spanish. The symbol to indicate it is ``~``.

  Example: ``Ky -- ri -- e~e -- lei -- son``

For more information about lilypond and its syntax see [lilypond.org](https://lilypond.org).

## Additional features

### Dump lyrics

Copy the lyrics of the score back to the text area. The new line characters are not preserved.

This function only works with a single voice.

### Merge

Some lilypond files have the lyrics spread over several sections. In those cases, to minimize the number of copy-paste operations you can copy all the sections one after the other and combine them together with the button **merge**. The exact behaviour depends on the number of voices of the score.

Example: in a score with three voices and two sections, the text

```
   section 1: {Hel -- lo } {Ho -- la} {Hal -- lo}
   section 2: {world} {mun -- do} {Welt}
```

becomes after pressing **merge**:

```
   {Hel -- lo world} {Ho -- la mun -- do} {Hal -- lo Welt}
```

### Clean

Lilypond lyrics code can contain comments and markup commands, etc. If you copy-paste from a lilypond file, you can delete all this extra code with the button **clean**.

Note that this function only deletes comments (marked with %), words that start with ``\`` or ``#``, and stanza numbers. It is very possible that other symbols have to been removed manually.

### Synalepha

It is possible to choose the elision symbol. The default is as described in MuseScore documentation, but you can choose the unicode undertie ``‿`` or a simple underscore ``_``.

None of these options translates exactly to MusicXML. See [musicxml reference](https://www.w3.org/2021/06/musicxml40/musicxml-reference/examples/elision-element/).

### Extenders

The extender lines can disabled unchecking the corresponding option.

### Blank space

To insert a space inside a syllable use the character ``_`` (underscore).

Example: ``1._Hel -- lo``

If for some reason you need to insert a white space instead of an _empty_ syllable, you can use the string ``" "`` (a space surrounded by double quotes). This is useful por example to stop the line extenders:

Example: ``Ky -- ri -- e __ _ _ " " _ ``

The syllable "e" is extended only along the next two notes.

Note that, apart from this special combination ``" "``, the double quote character is treated as any other character.

### Stanzas

Numbered verses (denoted in lilypond with ``\set stanza = "n."``) are appended to the next syllable

Example: ``\set stanza = "1." Hel -- lo`` is translated to ``1._Hel -- lo``

This functionallity is applied with the buttom **Clean**.

### Condensed notation

If you are not confortable with the lilypond syntax there is an alternative notation more readable that uses single hyphens for melismas inside words, and spaces for melismas at the end of a word (as many as notes in the melisma):

Example: ``Ky-ri--e   e--lei-son`` corresponds to ``Ky -- ri -- _ e _ _ e -- _ lei -- son``

You can switch between these two types of syntax with the buttons **condense** and **expand**.

To apply the lyrics to the score the lyrics **MUST** be in lilypond syntax (aka expanded).

### Slurs 

Slurs are ignored. I do not know how to access slurs and its properties inside the MuseScore API.

### Skip ties

Normally this option should be always cheched.

But if you are copy-pasting from a lilypond file created with an ancient version (or converted from an ancient version) of lilypond you should uncheck this option.

##### Lilypond files that need "Skip ties" unchecked
* Version 1.4
* files that contain the uncommented line ``melismaBusyProperties = #'()``

### Untie

When MuseScore imports a MIDI file with a time signature of 4/4 or similar, some notes that cross the third beat are transcribed as two tied notes. This is standard practice. But in some styles of music, for instance Renaissance polyphony, it is clearer if these notes are transcribed as dotted notes.

The buttons **Untie** and **Untie all** perform this conversion in one or all voices respectively.

This operation is mandatory with ancient lilypond files (see section "Skip ties").


## Motivation

The main reason to write this plugin was to help in converting to MuseScore the scores of vocal music in the site [tomasluisdevictoria.org](https://www.uma.es/victoria) devoted mainly to Spanish Renaissance music.

#### Workflow to convert these scores
* Import the .mid files in MuseScore unchecking the options "Clef changes" and "Show tempo text"
* Change the clef of the tenor to treble octave lower
* In the .ly file copy the section which contains the lyrics and paste it in the text area of this plugin
* Uncheck "skip ties" if necessary
* Press **Untie all**
* Press **Clean**
* Press **Paste**
* Do aditional editing: title, double bars, etc.
* Save file as .mscz
 

This plugin is inspired by _roblyrics_.



