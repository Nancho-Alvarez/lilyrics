import QtQuick 2.1
import QtQuick.Controls 1.0
import MuseScore 3.0
//import FileIO 3.0

MuseScore {
    menuPath: "Plugins.MelismaBusy"
    description: "Fix melisma"
    version: "1.0"
    requiresScore: true
    pluginType: "dialog"
    
    
    id:window
    width: 800; height: 400;
    
    onRun: {}
    
    // TEXT AREA
    TextArea {
        id:textLily
        anchors.left: window.left
        anchors.top: window.top
        anchors.bottom: buttonCancel.top
        anchors.right: buttonCancel.right
        anchors.topMargin: 10
        anchors.bottomMargin: 10
        anchors.leftMargin: 10
        width:parent.width
        wrapMode: TextEdit.WrapAnywhere
        textFormat: TextEdit.PlainText
        text: ""
        }

    
    
    // CANCEL     
    Button {
        id : buttonCancel
        text: qsTr("Cancel")
        anchors.bottom: window.bottom
        anchors.right: window.right
        anchors.topMargin: 10
        anchors.bottomMargin: 10
        anchors.rightMargin: 10
        onClicked: Qt.quit();
    }
       
    // TRANSLATE
    Button {
        id : buttonMelisma
        text: qsTr("Melisma busy")
        anchors.bottom: window.bottom
        anchors.right: buttonCancel.left
        anchors.topMargin: 10
        anchors.bottomMargin: 10
        anchors.rightMargin: 25
        width: 150
        onClicked: melisma();
    }
       

    
    function countNotes(track) {
        var notes=0;
        var cursor=curScore.newCursor();
        cursor.track=track;
        cursor.rewind(Cursor.SCORE_START);
        while(cursor.segment) {
            if (cursor.element.type==Element.CHORD) notes++;
            cursor.next();
        }
        return notes; 
    }
    
    function findTies(track) {
        var ties=[];
        var cursor=curScore.newCursor();
        cursor.track=track;
        cursor.rewind(Cursor.SCORE_START);
        var i=0;
        while(cursor.segment) {
            if (cursor.element.type==Element.CHORD) {
                var note=cursor.element.notes[0];
                if (note.tieBack != null) ties.push(i);
                i++;
            }
            cursor.next();
        }
        return ties; 
    }
    
    function findSyllables(s) {
        s=s.replace(/" "/g,"\"_\"");
        s=s.replace(/"  "/g,"\"__\"");
        var t=[];
        var inWord=false;
        var l=s.length;
        var b; // beginning of word
        var i=0;
        while (true) {
            var word="";
            if (i==l) break;
            var c=s[i];
            if (" \n\t".includes(c)) { i++; continue; }
            if (c=="\\" || c=="#") {
                i++;
                while(i<l && !" \n\t\\%".includes(s[i])) i++;
                continue;
            }
            if (c=="%") {
                i++;
                while(i<l && s[i]!="\n") i++;
                continue;
            }
            // beginning of a word
            b=i;
            word+=c;
            i++
            while (i<l && !" \n\t#\\%".includes(s[i])) {
                word+=s[i];
                i++;
            }
            // if it is a stanza, fastforward to the second quote
            if (word.startsWith("stanza")) {
                i-=word.length;
                var quot=0;
                while (quot<2) {
                    if (s[i]=="\"") quot++;
                    i++;
                }
                continue;
            }
            if (word=="--" || word=="__" ) continue;
            t.push(b);
        }
        return t;
    }
    
    
    function ties2dots(track) {
        var cursor=curScore.newCursor();
        cursor.track=track;
        cursor.rewind(0);
        while(cursor.segment) {
            if(cursor.element.type!=Element.CHORD) {
                cursor.next();
                continue;
            }
            var note=cursor.element.notes[0];
            if (note.tieForward) {
                var dur1=cursor.element.duration.ticks;
                cursor.next();
                var note2=cursor.element.notes[0];
                var dur2=cursor.element.duration.ticks;
                if (dur1==2*dur2) {
                    curScore.selection.select(note2);
                    cmd("delete");
                    curScore.selection.select(note);
                    // It seems that the "default" duration
                    // is the one of the note just deleted, not the current one,
                    // for that reason I have to do inc-duration-dotted 3 times
                    cmd("inc-duration-dotted");
                    cmd("inc-duration-dotted");
                    cmd("inc-duration-dotted");
               }
            }
            cursor.next();
        }
        curScore.selection.clear();
    }
    
    
    function melisma() {
        var s=textLily.text;
        var offsets=[];
        var pre=[];
        const re = /{[^}]*}/g;
        var match;
        var ss=[];
        var i=0;
        while ((match = re.exec(s)) !== null) {
            var m=match[0];
            var index=match.index;
            ss.push(m);
            offsets.push(index+1);
            pre.push(s.substring(i, index+1));
            i=index+m.length - 1;
        }
        pre.push(s.substring(i)); // last one
        if (ss.length==0) {
            ss=["{" + s + "}"];
            offsets=[0];
        }
        var startTrack=0;
        var endTrack=(ss.length-1)*4;
        var reconstructedS="";
        for (i=startTrack; i<=endTrack; i+=4) {
            s=ss[i/4];
            s=s.substring(1,s.length-1);
            var n1=countNotes(i);
            ties2dots(i);
            var n2=countNotes(i);
            
            var tokens=findSyllables(s);
            if (n2!=tokens.length) {
                console.log("ERROR staff:", i/4+1);
                console.log("notes: ", n1, "->", n2);
                console.log("syllables: ", tokens.length);
                return;
            }
            if (n2!=tokens.length) success=false;
            var ties=findTies(i);
            ties.reverse();
            var sArray=s.split("");
            for (var t=0; t<ties.length; t++) {
                console.log(tokens[ties[t]]);
                sArray.splice(tokens[ties[t]], 2)
            }
            s=sArray.join("");
            console.log("track:",i, " pre:", pre[i/4]);
            reconstructedS+=pre[i/4] + s;
        }
        reconstructedS+=pre[i/4];
        textLily.text=reconstructedS + "\nSUCCESS";
        textLily.select(0, reconstructedS.length);
    }
}
